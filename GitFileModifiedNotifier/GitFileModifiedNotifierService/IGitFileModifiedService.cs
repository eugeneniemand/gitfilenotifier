﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GitFileModifiedNotifierService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGitFileModifiedService" in both code and config file together.
    [ServiceContract]
    public interface IGitFileModifiedService
    {
        [OperationContract]
        Dictionary<string, GitFileInfo> AreFilesInUse(string UserName, List<string> Files);

        [OperationContract]
        Dictionary<string, GitFileInfo> GetFilesInUse();
        // TODO: Add your service operations here
    }
}
