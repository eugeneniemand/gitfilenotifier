﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GitFileModifiedNotifierService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public struct GitFileInfo
    {
        public string userName;
        public string fileName;
        public DateTime dateAdded;
        public DateTime lastUpdated;
    }

    public class GitFileModifiedService : IGitFileModifiedService
    {
        public static Dictionary<string, GitFileInfo> allUsersModifiedFiles = new Dictionary<string, GitFileInfo>();

        public Dictionary<string, GitFileInfo> AreFilesInUse(string userName, List<string> userModifiedFiles)
        {
            try
            {
                Dictionary<string, GitFileInfo> filesInConflictState = new Dictionary<string, GitFileInfo>();

                RemoveStaleFiles(userName, userModifiedFiles);
                
                foreach (string fileName in userModifiedFiles)
                {
                    string dictKey = GetDictKey(userName, fileName);
                    
                    if (fileIsInConflictState(userName, fileName))
                    {
                        filesInConflictState.Add(dictKey, allUsersModifiedFiles.Where(g => g.Value.fileName == fileName).First().Value);
                        if (!allUsersModifiedFiles.ContainsKey(dictKey))
                            allUsersModifiedFiles.Add(dictKey, new GitFileInfo() { userName = userName, fileName = fileName, dateAdded = DateTime.Now, lastUpdated = DateTime.Now });
                    }
                    else if (allUsersModifiedFiles.ContainsKey(dictKey) && allUsersModifiedFiles[dictKey].userName == userName)
                    {
                        UpdateCurrentFilesInUse(userName, fileName);
                    }
                    else
                        allUsersModifiedFiles.Add(dictKey, new GitFileInfo() { userName = userName, fileName = fileName, dateAdded = DateTime.Now, lastUpdated = DateTime.Now });
                }                
                return filesInConflictState;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Removes files from allUsersModifiedFiles for a user if the are no longer in a modified state
        /// </summary>
        private static void RemoveStaleFiles(string userName, List<string> userModifiedFiles)
        {
            // Create a copy of the allUsersModifiedFiles dictionary to allow removing of objects file iterating collection
            Dictionary<string, GitFileInfo> tmpAllUsersModifiedFiles = new Dictionary<string, GitFileInfo>(allUsersModifiedFiles);

            // Get all modified files for userName thats currently in allUsersModifiedFiles (using copy)
            foreach (GitFileInfo gfi in tmpAllUsersModifiedFiles.Values.Where(g => g.userName == userName))
            {
                // If the new list of userModifiedFiles does not contain the filename then its no 
                // longer in a modified state so it should be removed from allUsersModifiedFiles 
                if (!userModifiedFiles.Contains(gfi.fileName))
                {
                    allUsersModifiedFiles.Remove(GetDictKey(userName, gfi.fileName));
                }
            }
        }

        /// <summary>
        /// As the same file can be modified by different users we need a unique key for the dictionaries. 
        /// The full file path + userName is good enough
        /// </summary>        
        private static string GetDictKey(string userName, string fileName)
        {
            string dicKey = fileName + userName;
            return dicKey;
        }

        /// <summary>
        /// If allUsersModifiedFiles contains a fileName for a different user 
        /// it means that its already being modfied and therefor its in a conflicting state
        /// </summary>
        private static bool fileIsInConflictState(string UserName, string file)
        {
            foreach (GitFileInfo gfi in allUsersModifiedFiles.Values)
            {
                if (gfi.fileName == file && gfi.userName != UserName)
                    return true;                
            }
            return false;
        }

        /// <summary>
        /// Update the lastUpdated field to show when last the users client has sent the latest list of modifications
        /// </summary>
        private static void UpdateCurrentFilesInUse(string userName, string fileName)
        {
            string dicKey = GetDictKey(userName, fileName);
            var gfi = allUsersModifiedFiles[dicKey];
            gfi.lastUpdated = DateTime.Now;
        }

        /// <summary>
        /// Returns all files that are modified by all users
        /// </summary>
        public Dictionary<string, GitFileInfo> GetFilesInUse()
        {
            try
            {
                return allUsersModifiedFiles;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
