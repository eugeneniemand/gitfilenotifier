﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GitFileModiedNotifier;
using gfmsc = GitFileModifiedNotifierTray.GitFileModifiedServiceClient;

namespace GitFileModifiedNotifierTray
{
    public partial class Form1 : Form
    {
        private gfmsc.GitFileModifiedServiceClient client;
        private GitFileModiedNotifier.GitFileModiedNotifier gfmn = new GitFileModiedNotifier.GitFileModiedNotifier();

        public Form1()
        {
            InitializeComponent();

            allInUseFilesGridView.ColumnCount = 4;
            allInUseFilesGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            allInUseFilesGridView.RowHeadersVisible = false;
            allInUseFilesGridView.Columns[0].Name = "File Name";
            allInUseFilesGridView.Columns[1].Name = "User Name";
            allInUseFilesGridView.Columns[2].Name = "Added";
            allInUseFilesGridView.Columns[3].Name = "Checked";
            allInUseFilesGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            allInUseFilesGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            allInUseFilesGridView.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            allInUseFilesGridView.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            allInUseFilesGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            conflictFilesGridView.ColumnCount = 4;
            conflictFilesGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            conflictFilesGridView.RowHeadersVisible = false;
            conflictFilesGridView.Columns[0].Name = "File Name";
            conflictFilesGridView.Columns[1].Name = "User Name";
            conflictFilesGridView.Columns[2].Name = "Added";
            conflictFilesGridView.Columns[3].Name = "Checked";
            conflictFilesGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            conflictFilesGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            conflictFilesGridView.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            conflictFilesGridView.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            conflictFilesGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            snoozeNotificationsTimer.Interval = Convert.ToInt32(snoozeInterval.Value * 60 * 1000);
            checkFilesTimer.Interval = Convert.ToInt32(refreshInterval.Value * 60 * 1000);
            checkFilesTimer.Enabled = true;

            notifyIcon1.BalloonTipClicked += notifyIcon1_BalloonTipClicked;
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Warning;

            checkConflicts();
        }

        private void checkFilesTimer_Tick(object sender, EventArgs e)
        {
            checkConflicts();
        }

        private void checkConflicts()
        {
            try
            {
                allInUseFilesGridView.Rows.Clear();
                conflictFilesGridView.Rows.Clear();

                client = new gfmsc.GitFileModifiedServiceClient();
                toolStripStatusLabel1.Text = "Connecting to:" + client.Endpoint.Address.Uri.ToString();
                this.Refresh();
                var currentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                Dictionary<string, gfmsc.GitFileInfo> ConflictFilesInUse = client.AreFilesInUse(currentUser, gfmn.ModifiedFiles().ToArray());
                Dictionary<string, gfmsc.GitFileInfo> AllInUseFiles = client.GetFilesInUse();
                toolStripStatusLabel1.Text = "Connected to:" + client.Endpoint.Address.Uri.ToString();
                this.Text = "Connected to:" + client.Endpoint.Address.Uri.ToString(); 
                this.Refresh();

                foreach (KeyValuePair<string, gfmsc.GitFileInfo> kvp in ConflictFilesInUse)
                {
                    string[] row = { kvp.Value.fileName, kvp.Value.userName, kvp.Value.dateAdded.ToString("yyyy-MM-dd HH:mm:ss"), kvp.Value.lastUpdated.ToString("yyyy-MM-dd HH:mm:ss") };
                    conflictFilesGridView.Rows.Add(row);
                }

                foreach (KeyValuePair<string, gfmsc.GitFileInfo> kvp in AllInUseFiles)
                {
                    string[] row = { kvp.Value.fileName, kvp.Value.userName, kvp.Value.dateAdded.ToString("yyyy-MM-dd HH:mm:ss"), kvp.Value.lastUpdated.ToString("yyyy-MM-dd HH:mm:ss") };
                    allInUseFilesGridView.Rows.Add(row);
                }

                // Notifications
                if (chkShowNotifications.Checked && !snoozeNotificationsTimer.Enabled && ConflictFilesInUse.Count != 0)
                {
                    //Count the number of files that are conflicting per extention 
                    var ExtentionGroups = ConflictFilesInUse.GroupBy(n => new FileInfo(Path.Combine(n.Value.fileName)).Extension)
                        .Select(group =>
                        new
                        {
                            ExtentionName = group.Key,
                            //Notices = group.ToList(),
                            Count = group.Count()
                        })
                        .OrderBy(g => g.ExtentionName);

                    //Create Notification Message
                    string groupCountsMessage = "";
                    bool newLine = false;
                    foreach (var extGrp in ExtentionGroups)
                    {
                        groupCountsMessage += string.Format("{0}\t({1})\t{2}", extGrp.ExtentionName, extGrp.Count, newLine ? "\\" : "");
                    }

                    notifyIcon1.BalloonTipTitle = string.Format("Conflicts Detected (Click to Snooze for {0} minutes)", snoozeInterval.Value.ToString());
                    notifyIcon1.BalloonTipText = string.Format("There are files currently being editied by other users\n{0}", groupCountsMessage);
                    notifyIcon1.ShowBalloonTip(10000);
                }
                toolStripStatusLabel1.Text = string.Format("Idle... Last Updated: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                client.Close();
            }
            catch (Exception ex)
            {
                toolStripStatusLabel1.Text = ex.Message;
                this.Refresh();
                Console.WriteLine(ex.Message);
            }
        }

        void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            snoozeNotificationsTimer.Enabled = true;
        }

        private void snoozeNotificationsTimer_Tick(object sender, EventArgs e)
        {
            snoozeNotificationsTimer.Enabled = false;
        }

        private void snoozeInterval_ValueChanged(object sender, EventArgs e)
        {
            snoozeNotificationsTimer.Interval = Convert.ToInt32(snoozeInterval.Value * 60 * 1000);
        }

        private void refreshInterval_ValueChanged(object sender, EventArgs e)
        {
            checkFilesTimer.Interval = Convert.ToInt32(refreshInterval.Value * 60 * 1000);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        void Form1_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            var window = MessageBox.Show("Are you sure you want to quit? Click No to minimize to tray", "Quit?", MessageBoxButtons.YesNo);
            if (window == DialogResult.No)
            {
                e.Cancel = true;
                this.Hide();
            }
            else
                e.Cancel = false;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }
    }
}
