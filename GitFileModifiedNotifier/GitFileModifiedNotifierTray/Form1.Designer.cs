﻿namespace GitFileModifiedNotifierTray
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.checkFilesTimer = new System.Windows.Forms.Timer(this.components);
            this.allInUseFilesGridView = new System.Windows.Forms.DataGridView();
            this.conflictFilesGridView = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chkShowNotifications = new System.Windows.Forms.CheckBox();
            this.refreshInterval = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.snoozeNotificationsTimer = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.snoozeInterval = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.allInUseFilesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conflictFilesGridView)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.refreshInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.snoozeInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // checkFilesTimer
            // 
            this.checkFilesTimer.Interval = 5000;
            this.checkFilesTimer.Tick += new System.EventHandler(this.checkFilesTimer_Tick);
            // 
            // allInUseFilesGridView
            // 
            this.allInUseFilesGridView.AllowUserToAddRows = false;
            this.allInUseFilesGridView.AllowUserToDeleteRows = false;
            this.allInUseFilesGridView.AllowUserToResizeRows = false;
            this.allInUseFilesGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.allInUseFilesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allInUseFilesGridView.Location = new System.Drawing.Point(12, 25);
            this.allInUseFilesGridView.Name = "allInUseFilesGridView";
            this.allInUseFilesGridView.Size = new System.Drawing.Size(984, 186);
            this.allInUseFilesGridView.TabIndex = 0;
            // 
            // conflictFilesGridView
            // 
            this.conflictFilesGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.conflictFilesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.conflictFilesGridView.Location = new System.Drawing.Point(11, 230);
            this.conflictFilesGridView.Name = "conflictFilesGridView";
            this.conflictFilesGridView.Size = new System.Drawing.Size(985, 187);
            this.conflictFilesGridView.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 420);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(117, 17);
            this.toolStripStatusLabel1.Text = "Status: Disconnected";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 214);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Files that are modified by you and other developers";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "All files that are in a modified state";
            // 
            // chkShowNotifications
            // 
            this.chkShowNotifications.AutoSize = true;
            this.chkShowNotifications.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkShowNotifications.Checked = true;
            this.chkShowNotifications.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowNotifications.Location = new System.Drawing.Point(882, 6);
            this.chkShowNotifications.Name = "chkShowNotifications";
            this.chkShowNotifications.Size = new System.Drawing.Size(114, 17);
            this.chkShowNotifications.TabIndex = 5;
            this.chkShowNotifications.Text = "Show Notifications";
            this.chkShowNotifications.UseVisualStyleBackColor = true;
            // 
            // refreshInterval
            // 
            this.refreshInterval.Location = new System.Drawing.Point(825, 3);
            this.refreshInterval.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.refreshInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.refreshInterval.Name = "refreshInterval";
            this.refreshInterval.Size = new System.Drawing.Size(40, 20);
            this.refreshInterval.TabIndex = 6;
            this.refreshInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.refreshInterval.ValueChanged += new System.EventHandler(this.refreshInterval_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(692, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Refresh Interval (minutes)";
            // 
            // snoozeNotificationsTimer
            // 
            this.snoozeNotificationsTimer.Interval = 10000;
            this.snoozeNotificationsTimer.Tick += new System.EventHandler(this.snoozeNotificationsTimer_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(498, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Snooze Interval (minutes)";
            // 
            // snoozeInterval
            // 
            this.snoozeInterval.Location = new System.Drawing.Point(631, 3);
            this.snoozeInterval.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.snoozeInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.snoozeInterval.Name = "snoozeInterval";
            this.snoozeInterval.Size = new System.Drawing.Size(40, 20);
            this.snoozeInterval.TabIndex = 8;
            this.snoozeInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.snoozeInterval.ValueChanged += new System.EventHandler(this.snoozeInterval_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 442);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.snoozeInterval);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.refreshInterval);
            this.Controls.Add(this.chkShowNotifications);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.conflictFilesGridView);
            this.Controls.Add(this.allInUseFilesGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += Form1_FormClosing;
            ((System.ComponentModel.ISupportInitialize)(this.allInUseFilesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conflictFilesGridView)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.refreshInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.snoozeInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Timer checkFilesTimer;
        private System.Windows.Forms.DataGridView allInUseFilesGridView;
        private System.Windows.Forms.DataGridView conflictFilesGridView;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkShowNotifications;
        private System.Windows.Forms.NumericUpDown refreshInterval;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer snoozeNotificationsTimer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown snoozeInterval;
    }
}

