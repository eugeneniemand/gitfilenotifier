﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GitFileModiedNotifier
{
    public class GitFileModiedNotifier
    {
        
        private string GetStatus()
        {
            ProcessStartInfo gitInfo = new ProcessStartInfo();
            gitInfo.CreateNoWindow = true;
            gitInfo.RedirectStandardError = true;
            gitInfo.RedirectStandardOutput = true;
            gitInfo.FileName = Properties.Settings.Default.GitExecPath + @"\bin\git.exe";
            gitInfo.UseShellExecute = false;

            Process gitProcess = new Process();
            gitInfo.Arguments = "status";
            gitInfo.WorkingDirectory = Properties.Settings.Default.RepositoryPaths;

            gitProcess.StartInfo = gitInfo;
            gitProcess.Start();

            string stderr_str = gitProcess.StandardError.ReadToEnd();  // pick up STDERR
            string stdout_str = gitProcess.StandardOutput.ReadToEnd(); // pick up STDOUT

            gitProcess.WaitForExit();
            gitProcess.Close();
            return stdout_str;
        }

        public List<string> ModifiedFiles()
        {
            List<string> modifiedFiles = new List<string>();
            string status = GetStatus();

            string pat = @"(#\tmodified: *)(.*)\n";

            // Instantiate the regular expression object.
            Regex r = new Regex(pat, RegexOptions.IgnoreCase & RegexOptions.Multiline);

            // Match the regular expression pattern against a text string.
            MatchCollection matches = r.Matches(status);
            foreach (Match match in matches)
            {
                if (match.Groups.Count == 3)
                    modifiedFiles.Add(match.Groups[2].Value);
            }

            return modifiedFiles;
        }       
    }
}
